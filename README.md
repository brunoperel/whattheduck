WhatTheDuck
===========

Free and open source Android app allowing simple operations on a DucksManager comic book collection.

Want to know a bit more about it ? [Have a look at the wiki !](../../wiki/What-The-Duck)
